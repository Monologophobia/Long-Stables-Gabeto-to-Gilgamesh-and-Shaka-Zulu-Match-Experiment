<?php
// DIC configuration

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// twig
$container['view'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    $view = new Slim\Views\Twig($settings['template_path'], array(
        'cache' => $settings['cache_path']
    ));
    return $view;
};

//Override the default Not Found Handler
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['view']->render($response->withStatus(404), '404.htm', [
            "title" => "Page Not Found"
        ]);
    };
};

// add csrf
$container['csrf'] = function ($c) {
    return new \Slim\Csrf\Guard;
};

// add flash messages
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

// add Swift Mail
$container['swiftmail'] = function ($c) {
    // smtp or sendmail?
    $transport = (new Swift_SmtpTransport('mail.enhancedhospitality.co.uk', 465, 'ssl'))->setUsername("websiteform@enhancedhospitality.co.uk")->setPassword("Camden-20178");
    //$transport = new Swift_SendmailTransport('/usr/sbin/sendmail -bs');
    return new Swift_Mailer($transport);
};

// register Form Controller
$container['Forms'] = function($c) {
    return new App\Controllers\Forms($c);
};
