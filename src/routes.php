<?php
// Routes
$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response, 'home.htm', [
        'title' => 'Home'
    ]);
});

$app->get('/christmas[/]', function ($request, $response, $args) {
    //return $response->withRedirect('/');
    return $this->view->render($response, 'christmas.htm', [
        'title' => 'Christmas'
    ]);
});

$app->get('/restaurant[/]', function ($request, $response, $args) {
    return $this->view->render($response, 'restaurant.htm', [
        'title' => 'Restaurant'
    ]);
});

$app->get('/bar[/]', function ($request, $response, $args) {
    return $this->view->render($response, 'bar.htm', [
        'title' => 'Bar'
    ]);
});

$app->get('/kings-club[/]', function ($request, $response, $args) {

    // generate CSRF details
    $nameKey  = $this->csrf->getTokenNameKey();
    $valueKey = $this->csrf->getTokenValueKey();
    $name     = $request->getAttribute($nameKey);
    $value    = $request->getAttribute($valueKey);

    return $this->view->render($response, 'kings-club.htm', [
        'title' => "King's Club",
        'csrf'  => [
            'nameKey'  => $nameKey,
            'valueKey' => $valueKey,
            'name'     => $name,
            'value'    => $value
        ],
        'guest_list_message' => $this->flash->getMessage('guest_list_message')[0],
        'club_bar_tables_message' => $this->flash->getMessage('club_bar_tables_message')[0]
    ]);

});

// guest list
$app->get('/kings-club/guest-list[/]', function($request, $response, $args) {
    return $response->withRedirect('/kings-club');
});
$app->post('/kings-club/guest-list', 'Forms:guestList');

// Club Bar Tables
$app->get('/kings-club/club-bar-tables[/]', function($request, $response, $args) {
    return $response->withRedirect('/kings-club');
});
$app->post('/kings-club/club-bar-tables', 'Forms:clubBarTables');

$app->get('/group-bookings[/]', function ($request, $response, $args) {
    return $this->view->render($response, 'group-bookings.htm', [
        'title' => 'Group Bookings'
    ]);
});

$app->get('/events', function ($request, $response, $args) {
    return $this->view->render($response, 'events.htm', [
        'title' => 'Events'
    ]);
});

$app->get('/contact-us[/]', function ($request, $response, $args) {

    // generate CSRF details
    $nameKey  = $this->csrf->getTokenNameKey();
    $valueKey = $this->csrf->getTokenValueKey();
    $name     = $request->getAttribute($nameKey);
    $value    = $request->getAttribute($valueKey);

    return $this->view->render($response, 'contact-us.htm', [
        'title' => 'Contact Us',
        'csrf'  => [
            'nameKey'  => $nameKey,
            'valueKey' => $valueKey,
            'name'     => $name,
            'value'    => $value
        ],
        'message' => $this->flash->getMessage('message')[0]
    ]);

});

// general contact form
$app->get('/contact-us/contact[/]', function($request, $response, $args) {
    return $response->withRedirect('/contact-us');
});
$app->post('/contact-us/contact', 'Forms:contactUs');

// vouchers
$app->get('/vouchers[/]', function($request, $response, $args) {
    return $this->view->render($response, 'vouchers.htm', [
        'title' => 'Vouchers',
    ]);
});

// Redirect old pages
$app->get('/home/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/');
});

$app->get('/restaurant/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/restaurant');
});

$app->get('/bar/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/bar');
});

$app->get('/kings-club/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/kings-club');
});

$app->get('/group-bookings/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/group-bookings');
});

$app->get('/gallery/restaurant[/]', function($request, $response, $args) {
    return $response->withRedirect('/restaurant');
});

$app->get('/gallery/lounge-bar[/]', function($request, $response, $args) {
    return $response->withRedirect('/bar');
});

$app->get('/gallery/kings-club[/]', function($request, $response, $args) {
    return $response->withRedirect('/kings-club');
});

$app->get('/whats-on[/]', function($request, $response, $args) {
    return $response->withRedirect('/events');
});

$app->get('/events/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/events');
});

$app->get('/contact-us/{old}[/]', function($request, $response, $args) {
    return $response->withRedirect('/contact-us');
});

$app->get('/privacy-policy[/]', function ($request, $response, $args) {
    return $this->view->render($response, 'privacy-policy.htm', [
        'title' => 'Privacy Policy'
    ]);
});

