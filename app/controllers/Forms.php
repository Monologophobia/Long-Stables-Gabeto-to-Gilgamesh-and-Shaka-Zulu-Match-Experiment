<?php namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Swift_Message;

class Forms {

	protected $container;
	protected $view;
	protected $message;
	protected $recipient;

	public function __construct(\Slim\Container $container) {
		$this->container = $container;
		$this->view      = $this->container->get("view");
		$this->message   = 'Thanks. We\'ll be in touch as soon as possible';
		$this->from      = "websiteform@enhancedhospitality.co.uk";
		$this->recipient = "info@shaka-zulu.com";
	}

	public function guestList($request, $response, $args) {
try {
        $guest_list = $request->getParam('guest_list');

		// honeypot
		if (!empty($guest_list['email'])) {
			return $this->redirectToPathWithMessage('/kings-club', $this->message, $response, 'guest_list_message');
		}

		// gather variables
		$name    = filter_var($guest_list['name'], FILTER_SANITIZE_STRING);
		$email   = filter_var($guest_list['emailaddress'], FILTER_SANITIZE_STRING);
		$phone   = filter_var($guest_list['phone'], FILTER_SANITIZE_STRING);
        $date    = filter_var($guest_list['date'], FILTER_SANITIZE_STRING);
		$males   = filter_var($guest_list['males'], FILTER_SANITIZE_STRING);
		$females = filter_var($guest_list['females'], FILTER_SANITIZE_STRING);

		if (!$name || !$email || !$phone || !$date || !$males || !$females) {
			return $this->redirectToPathWithMessage('/kings-club', 'Some information was missing. Please try again.', $response, 'guest_list_message');
		}

		$body    = "Name: $name\r\nEmail: $email\r\nPhone: $phone\r\nDate: $date\r\nMales: $males\r\nFemales: $females";
		$message = (new Swift_Message())
  					->setSubject('Shaka Zulu Guest List Submission')
					->setFrom($this->from)
					->setTo($this->recipient)
					->setBody($body);

		$this->container->swiftmail->send($message);

		return $this->redirectToPathWithMessage('/kings-club', $this->message, $response, 'guest_list_message');
}
catch (\Exception $e) {
return $this->redirectToPathWithMessage('/kings-club', $e->getMessage(), $response, 'guest_list_message');
}
	}

    public function clubBarTables($request, $response, $args) {

try {
        $club_bar_tables = $request->getParam('club_bar_tables');

		// honeypot
		if (!empty($club_bar_tables['email'])) {
			return $this->redirectToPathWithMessage('/kings-club', $this->message, $response, 'club_bar_tables_message');
		}

		// gather variables
		$name       = filter_var($club_bar_tables['name'], FILTER_SANITIZE_STRING);
		$email      = filter_var($club_bar_tables['emailaddress'], FILTER_SANITIZE_STRING);
		$phone      = filter_var($club_bar_tables['phone'], FILTER_SANITIZE_STRING);
        $date       = filter_var($club_bar_tables['date'], FILTER_SANITIZE_STRING);
		$party_size = intval($club_bar_tables['party_size']);
		$message    = filter_var($club_bar_tables['message'], FILTER_SANITIZE_STRING);

		if (!$name || !$email || !$phone || !$date || !$party_size || !$message) {
			return $this->redirectToPathWithMessage('/kings-club', 'Some information was missing. Please try again.', $response, 'club_bar_tables_message');
		}

		$body    = "Name: $name\r\nEmail: $email\r\nPhone: $phone\r\nDate: $date\r\nParty Size: $party_size\r\nMessage: $message";
		$message = (new Swift_Message())
  					->setSubject('Shaka Zulu Club Bar Tables Submission')
					->setFrom($this->from)
					->setTo($this->recipient)
					->setBody($body);

		$this->container->swiftmail->send($message);

		return $this->redirectToPathWithMessage('/kings-club', $this->message, $response, 'club_bar_tables_message');
}
catch (\Exception $e) {
return $this->redirectToPathWithMessage('/kings-club', $e->getMessage(), $response, 'club_bar_tables_message');
}

	}

	public function contactUs($request, $response, $args) {
try {
		// honeypot
		if (!empty($request->getParam('email'))) {
			return $this->redirectToPathWithMessage('/contact-us', $this->message, $response);
		}

		// gather variables
		$name     = filter_var($request->getParam('name'), FILTER_SANITIZE_STRING);
		$email    = filter_var($request->getParam('emailaddress'), FILTER_SANITIZE_STRING);
		$phone    = filter_var($request->getParam('phone'), FILTER_SANITIZE_STRING);
		$subject  = filter_var($request->getParam('subject'), FILTER_SANITIZE_STRING);
		$message  = filter_var($request->getParam('message'), FILTER_SANITIZE_STRING);

		if (!$name || !$email || !$phone || !$message) {
			return $this->redirectToPathWithMessage('/contact-us', 'Some information was missing. Please try again.', $response);
		}

		$body    = "Name: $name\r\nEmail: $email\r\nPhone: $phone\r\n\r\nMessage: $message";
		$message = (new Swift_Message())
  					->setSubject($subject)
					->setFrom($this->from)
					->setTo($this->recipient)
					->setBody($body);

		$this->container->swiftmail->send($message);

		return $this->redirectToPathWithMessage('/contact-us', $this->message, $response);
}
catch (\Exception $e) {
return $this->redirectToPathWithMessage('/contact-us', $e->getMessage(), $response);
}

	}

	public function giftCards($request, $response, $args) {

		// honeypot
		if (!empty($request->getParam('email'))) {
			return $this->redirectToPathWithMessage('/gift-cards', $this->message, $response);
		}

		// gather variables
		$name      = filter_var($request->getParam('name'), FILTER_SANITIZE_STRING);
		$email     = filter_var($request->getParam('emailaddress'), FILTER_SANITIZE_STRING);
		$phone     = filter_var($request->getParam('phone'), FILTER_SANITIZE_STRING);
		$value     = filter_var($request->getParam('value'), FILTER_SANITIZE_NUMBER_INT);
		$r_name    = filter_var($request->getParam('r_name'), FILTER_SANITIZE_STRING);
		$r_address = filter_var($request->getParam('r_address'), FILTER_SANITIZE_STRING);
		$message   = filter_var($request->getParam('message'), FILTER_SANITIZE_STRING);
		$promo     = filter_var($request->getParam('promo'), FILTER_SANITIZE_STRING);

		if (!$name || !$email || !$phone || !$r_name || !$r_address || !$value) {
			return $this->redirectToPathWithMessage('/gift-cards', 'Some information was missing. Please try again.', $response);
		}

		$body  = "Value: $value\r\nYour Name: $name\r\nEmail: $email\r\nPhone: $phone\r\n";
		$body .= "Recipient Name: $r_name\r\nAddress: $r_address\r\nMessage: $message\r\n";
		$body .= "Promo Code: $promo";
		$message = (new Swift_Message())
  					->setSubject('Shaka Zulu Gift Card Submission')
					->setFrom($this->from)
					->setTo($this->recipient)
					->setBody($body);

		$this->container->swiftmail->send($message);

        return $this->redirectToPathWithMessage('/gift-cards', $this->message, $response);

	}

	/**
	 * Redirects to a specified path with a specified message
	 * @param String $path
	 * @param String $message
	 * @param Response Object
     * @param String Optional Message Key - Defaults to 'message'
	 * @return $response->withRedirect() Object
	 */
	private function redirectToPathWithMessage($path, $message, $response, $message_key = 'message') {
		$this->container->flash->addMessage($message_key, $message);
		return $response->withRedirect($path);
	}

}
