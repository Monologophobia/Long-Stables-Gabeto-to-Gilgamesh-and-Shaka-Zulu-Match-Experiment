// register interactable elements
var header     = document.getElementsByTagName('header')[0];
var book_now   = document.getElementById('book-now');
var open_nav   = document.getElementById('open-nav');
var close_nav  = document.getElementById('close-nav');
var rotators   = document.getElementsByClassName('rotator');

// register click handlers
book_now.addEventListener("click", function() {
    close_nav.click();
    var position = document.getElementById('booking-form').getBoundingClientRect();
    var top = window.scrollY + position.top - header.clientHeight - 50;
    console.log(top);
    scrollToY(top);
});

open_nav.addEventListener("click", function(e) {
    e.preventDefault();
    document.body.classList.toggle('nav');
});

close_nav.addEventListener("click", function(e) {
    e.preventDefault();
    document.body.classList.remove('nav');
});

document.addEventListener("DOMContentLoaded", function() {
    headerBackground();
    nextTopImage();
});

window.onscroll = function() {
    headerBackground();
    parallax();
}

function headerBackground(scroll_y) {
    var scroll_y      = getWindowScrollY();
    var header_height = header.clientHeight;
    if (scroll_y > header_height) {
        header.classList.add('background');
    }
    else {
        header.classList.remove('background');
    }
}

function nextTopImage() {
    if (rotators && rotators.length > 0) {
        for (var i = 0; i < rotators.length; i++) {
            var paginators = getPaginatorsForRotator(rotators[i]);
            if (paginators && paginators.length > 0) {

                var next = 0;
                for (var x = 0; x < paginators.length; x++) {
                    if (paginators[x].classList.contains('active')) next = x + 1;
                    
                }
                if (next >= paginators.length) next = 0;

                loadTopImage(paginators[next]);

            }
        }
    }
}

function getPaginatorsForRotator(rotator) {
    if (rotator) {
        var paginator = rotator.getElementsByClassName('rotator-pagination')[0];
        if (paginator) {
            return paginator.getElementsByTagName('a');
        }
    }
    return false;
}

function findAncestor (el, sel) {
    while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el,sel)));
    return el;
}

var timer = false;
function loadTopImage(element = false) {
    var rotator = findAncestor(element, '.rotator');
    if (rotator) {
        if (!element) return;

        // get the element item and the list of images
        var item   = element.dataset.item;
        var images = JSON.parse(rotator.dataset.images);

        if (images && images.length > 0) {

            // clear any timers
            if (timer) clearInterval(timer);

            // set up the variables
            var title         = rotator.dataset.title;
            var loaded_images = rotator.getElementsByTagName('img');
            var paginators    = getPaginatorsForRotator(rotator);
            var loaded        = false;

            // if there are any images already loaded, see if the specific one we want is loaded
            // and remove active class from everything
            if (loaded_images && loaded_images.length > 0) {
                for (var i = 0; i < loaded_images.length; i++) {
                    // only allow direct descendants of the rotator
                    if (loaded_images[i].parentNode == rotator) {
                        loaded_images[i].classList.remove('active');
                        if (loaded_images[i].dataset.item == item) {
                            loaded = loaded_images[i];
                        }
                    }
                }
            }

            // if it is, mark it active
            // if it isn't, add it
            if (loaded) {
                loaded.classList.add('active');
            }
            else {
                rotator.insertAdjacentHTML( 'afterbegin', '<img src="' + images[item] + '" alt="' + title + '" data-item="' + item + '" class="active" />' );
                parallax();
            }

            // clear the paginators and select the clicked one
            for (var i = 0; i < paginators.length; i++) paginators[i].classList.remove('active');
            element.classList.add('active');

            timer = setInterval(nextTopImage, 10000);

        }

    }
}

function parallax() {
    if (rotators && rotators.length > 0) {
        for (var x = 0; x < rotators.length; x++) {
            var images = rotators[x].getElementsByTagName('img');
            if (images && images.length > 0) {
                var calc = calculateTranslateY(rotators[x]);
                for (var i = 0; i < images.length; i++) {
                    images[i].style.transform = "translateY(" + calc + ") translateX(-50%)";
                }
            }
        }
    }
}

function offset(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

function calculateTranslateY(rotator) {
    var scroll_y   = getWindowScrollY();
    var percentage = "-50%";
    if (rotator.classList.contains('body-rotator')) {
        scroll_y   = scroll_y - offset(rotator).top + window.innerHeight;
        percentage = "-100%";
    }
    return "calc(" + percentage + " + " + scroll_y / 2 + "px)";
}

function scrollEasing(position) {
    return Math.sin(position * (Math.PI / 2));
}

function scrollToY(y_position) {

    var scrollY     = window.scrollY || document.documentElement.scrollTop;
    var y_position  = y_position || 0;
    var speed       = 3000;
    var currentTime = 0;

    // min time .1, max time .8 seconds
    var time = Math.max(.1, Math.min(Math.abs(scrollY - y_position) / speed, .8));

    // add animation loop
    function tick() {

        currentTime += 1 / 60;

        var p = currentTime / time;
        var t = scrollEasing(p);

        if (p < 1) {
            window.requestAnimationFrame(tick);
            window.scrollTo(0, scrollY + ((y_position - scrollY) * t));
        } else {
            window.scrollTo(0, y_position);
        }

    }

    tick();

}

function getWindowScrollY() {
    return window.pageYOffset || document.documentElement.scrollTop;
}

function handleCapture() {
    if (document.cookie.replace(/(?:(?:^|.*;\s*)signup\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "true") {
        var modal = document.getElementById('modal-capture');
        if (modal) {
            var date  = new Date();
            var hours = 24;
            date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
            document.cookie = "signup=true; expires=" + date.toGMTString();
            modal.classList.add('active');
        }
    }
}

window.addEventListener('load', function(e) {
    handleCapture();
    var modal_close = document.querySelector('#modal-capture #close');
    if (modal_close) {
        modal_close.addEventListener('click', function(e) {
            e.preventDefault();
            var modal = document.getElementById('modal-capture');
            if (modal) {
                modal.classList.remove('active');
            }
        });
    }
});